--ACCESS=access content
connection:
  url: 'data.wa.gov'
  dsid: 'Fill in your dataset Id'
select:
  origin:
  filer_name:
  type: filer_type
  jurisdiction:
  filing_method:
  url:
  election_year:
  report_number:
  filer_id:
  case(position is not null,position,position is null, 'NA'): filer_position

where:
 AND:
  AND:
   OR:
    # --IF=:report_number
    report_number:
      { operator : '=', value: :report_number}
    # --END
    # --IF=:filer_id
    filer_id:
     { operator : '=', value: :filer_id}
    # --END
   # --IF=:filing_method
   filing_method:
    { operator : '=', value: :filing_method}
   # --ELSE
   filing_method:
    { operator : '=', value: 'Electronic'}
   # --END
   # --IF=:election_year
   election_year:
    { operator : '>=', value: :election_year}
   # --ELSE
   election_year:
    { operator : '>=', value: '2017'}
   # --END
  OR:
# --IF=:origin
   origin:
    { operator : '=', value: :origin}
# --END
# --IF=:type
   type:
    { operator : '=', value: :type}
# --END
limit:
  - 50
order:
  - { column: election_year, order: DESC }
  - { column: jurisdiction, order: ASC }
  - { column: filer_name, order: ASC }
