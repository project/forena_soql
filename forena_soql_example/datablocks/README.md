
        This module is a plugin for forena reports that use Socrata as open data source
        Copyright (C) 2017  Jim Coleman, Shravanthi Rajagopal and Manasi Chaudari 
        Public Disclosure Commission
        

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at your option) any later version.
        

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
    
Sample Datablocks for reference

Kindly fill in your dataset Id and change column names accordingly to proceed