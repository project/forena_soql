--ACCESS=access content
connection:
  url: 'data.wa.gov'
  dsid: 'Fill in your dataset Id'
select:
  - origin
  - filer_name
  - type
  - jurisdiction
  - filing_method
  - url
  - election_year

where:
 AND:
# --IF=:filing_method
  filing_method:
   {operator: '=', value: :filing_method}
# --ELSE
  filing_method:
   {operator: '=', value: 'Other'}
# --END
# --IF=:election_year
  election_year:
    { operator : '=', value: :election_year}
# --ELSE
  election_year:
    { operator : '=', value: '2012'}
# --END

limit:
  - 100

order:
  - { column: election_year, order: DESC }
