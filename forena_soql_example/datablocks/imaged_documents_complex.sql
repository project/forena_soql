--ACCESS=access content
connection:
  url: 'data.wa.gov'
  dsid: 'Fill in your dataset Id'
select:
  - origin
  - filer_name
  - type
  - jurisdiction
  - filing_method
  - url
  - election_year
  - report_number
  - filer_id

where:
 AND:
  AND:
   OR:
    # --IF=:report_number
    report_number:
      { operator : '=', value: :report_number}
    # --END
    # --IF=:filer_id
    filer_id:
     { operator : '=', value: :filer_id}
    # --END
   # --IF=:filing_method
   filing_method:
    { operator : '=', value: :filing_method}
   # --ELSE
   filing_method:
    { operator : '=', value: 'Other'}
   # --END
   # --IF=:election_year
   election_year:
    { operator : '>=', value: :election_year}
   # --ELSE
   election_year:
    { operator : '>=', value: '2014'}
   # --END
  OR:
    # --IF=:origin
      origin:
      { operator : '=', value: :origin}
    # --END
    # --IF=:type
      type:
        { operator : '=', value: :type}
    # --END

limit:
  - 100

order:
  - { column: election_year, order: DESC }