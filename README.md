
        This module is a plugin for forena reports that use Socrata as open data source
        Copyright (C) 2017  Jim Coleman, Shravanthi Rajagopal and Manasi Chaudari 
        Public Disclosure Commission
        

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at your option) any later version.
        

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
    
FORENA SOQL module

Usage instructions for connecting to a Socrata dataset:

The traditional SQL statement needs to be translated into YAML syntax, and placed in a Forena Reports datablock file with all your other datablock files. At present, the file extension should be .sql, even though the contents are YAML.

Here is the structure:

--ACCESS=access content
connection:
  url: 'data.wa.gov'
  dsid: 'XXXX-XXXX' --Your Socrata dataset ID
select:
  - column_1
  - column_2
  - sum(column_3)
order:
  - { column: column_1, order: ASC }
group:
  - class_title
search: 'Inslee for governor'


Usage notes:

SELECT: Place column names in a sequence array, one name per line, following a '- '.

FORENA PARAMETERS: See "WHERE" information below.

ORDER: The 'order by' part of your SQL statement must come before the 'group' (if you are using GROUP BY aggregation) and must be in the following array format:
  - { column: column_1, order: ASC }
  - { column: column_2, order: ASC }

GROUP: Group (equivalent to GROUP BY) must come after the ORDER part of the query. Usage is as such:
group:
  - column_1
  - column_2

LIMIT: Usage:
  - 5

search: A string surrounded by quotes. Preforms a full text search on the entire dataset.

WHERE: Where statements must finally be constructed into a single line. 
We can use AND or OR or nested ANDs / ORs / combination of both in the statement such that by using 'FrxSocrataSOQ.inc' we can convert it into a single string.

The general structure is as follows:

```
where:
 AND/OR:
    AND:
        OR:
            columnName : { operator: [=,<=,>=,<,>] , value: actualValue}
    columnName : { operator: [=,<=,>=,<,>] , value: actualValue
    columnName : { operator: [=,<=,>=,<,>] , value: actualValue}
 OR:
    columnName : { operator: [=,<=,>=,<,>] , value: actualValue}
    columnName : { operator: [=,<=,>=,<,>] , value: actualValue}
 ```

If same column has multiple conditions associated with it, the format becomes as follows:

```
where:
 OR:
    columnName : [{ operator: [=,<=,>=,<,>] , value: actualValue}, 
                    { operator: [=,<=,>=,<,>] , value: actualValue}]
 ```
where a list of operator-value pairs are added to same column name.

We can also assign variables to columns, for which we will make use of IF ELSE.

We can use either IF ELSE END or just IF END

'#' is included at front to make it get considered as comment in Symfony YAML Parsing.

Syntax:
```
 # --IF=:columnName
 columnName:
    { operator : '=', value: :variable}
   # --ELSE
   columnName:
    { operator : '=', value: 'defaultValue'}
   # --END
```

   
One complex example:

```
where:
 AND:
  AND:
   OR:
    # --IF=:report_number
    report_number:
      { operator : '=', value: :report_number}
    # --END
    # --IF=:filer_id
    filer_id:
     { operator : '=', value: :filer_id}
    # --END
   # --IF=:filing_method
   filing_method:
    { operator : '=', value: :filing_method}
   # --ELSE
   filing_method:
    { operator : '=', value: 'Electronic'}
   # --END
   # --IF=:election_year
   election_year:
    { operator : '>=', value: :election_year}
   # --ELSE
   election_year:
    { operator : '>=', value: '2017'}
   # --END
  OR:
# --IF=:origin
   origin:
    { operator : '=', value: :origin}
# --END
# --IF=:type
   type:
    { operator : '=', value: :type}
# --END
```

Required Configuration:

The Socrata App token id should be set in Configuration/Content Authoring/Forena Reports/Forena SoQL token settings once the module is installed

References:
https://github.com/allejo/PhpSoda/wiki/Working-with-SoQL-Queries
