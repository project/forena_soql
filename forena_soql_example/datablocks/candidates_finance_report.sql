
--ACCESS=access content
connection:
  url: 'data.wa.gov'
  dsid: 'Fill in your dataset Id'
select:
  - origin
  - filer_name
  - type
  - jurisdiction
  - filing_method
  - url
  - election_year
where:
 AND:
# --IF=:filing_method
  filing_method:
   {operator: '=', value: :filing_method}
# --ELSE
  filing_method:
   {operator: '=', value: 'Electronic'}
# --END
# --IF=:election_year
  election_year:
    { operator : '=', value: :election_year}
   # --ELSE
  election_year:
    { operator : '=', value: '2017'}
   # --END
limit:
  - 50
order:
  - { column: election_year, order: DESC }
  - { column: jurisdiction, order: ASC }
  - { column: filer_name, order: ASC }
