<?php
/*
Plugin that converts YAML file to Forena Report/Datablock
        Copyright (C) 2017  Jim Coleman, Shravanthi Rajagopal and Manasi Chaudari, Public Disclosure Commission

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
*/
use allejo\Socrata\SodaClient;
use allejo\Socrata\SodaDataset;
use allejo\Socrata\SoqlQuery;
use allejo\Socrata\SoqlOrderDirection;
use Symfony\Component\Yaml\Yaml;
use allejo\Socrata\Exceptions\SodaException;
use allejo\Socrata\Exceptions\InvalidResourceException;
use allejo\Socrata\Exceptions\HttpException;
use allejo\Socrata\Exceptions\CurlException;
use allejo\Socrata\Exceptions\FileNotFoundException;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * @file
 *
 * Data provider for Socrata's dataset SODA SoQL API.
 *
 */
/*class FrxSocrataSOQL  extends FrxDataSource { */

class FrxSocrataSOQL extends FrxDataSource
{
    private $db;
    private $dataset_id;
    private $client;

    /**
     * Object constructor
     * @param unknown_type $uri Database connection string.
     * @param string $repos_path Path to location of data block definitions
     */
    public function __construct($conf, $repos_path, $name)
    {
        parent::__construct($conf, $repos_path, $name);
        $page_results = null;

        // Set up items required to translate.
        $this->te = new FrxSyntaxEngine(FRX_SQL_TOKEN, ':', $this);
    }

    /**
     * Get data based on file data block in the repository.
     *
     * @param String $block_name
     * @param Array $parm_data
     * @param Query $subQuery
     */
    public function sqlData($yaml, $options = array())
    {
        $this->types = isset($options['type']) ? $options['type'] : array();
        try {
            $yaml = $this->te->replace($yaml);

            //echo "<script>console.log( $yaml);</script>";
            $soql_yaml = Yaml::parse($yaml);

            $dataset_id = $soql_yaml['connection']['dsid'];
            $url = $soql_yaml['connection']['url'];

            // This is the 'InfoSearch' app token registered with Socrata
            $app_token = variable_get("forena_soql_app_token");

            // Load the types array based on data
            //$this->types = isset($options['type']) ? $options['type'] : array();

            if ($yaml) {

                $sc = new SodaClient($url, $app_token);

                // Access a dataset based on the API end point
                $ds = new SodaDataset($sc, $dataset_id);

                // Create a SoqlQuery that will be used to filter out the results of a dataset
                $soql2 = new SoqlQuery();


                $has_order = 0;

                if (isset($soql_yaml['select'])) {

                    $soql2->select($soql_yaml['select']);
                } else {
                    $soql2->select();
                }
                if (isset($soql_yaml['where'])) {
                    // Populate the where clause from $item array
                    $soql2->where($this->formWhereClause($soql_yaml['where'], ''));
                }

                if (isset($soql_yaml['limit'])) {
                    // Populate the LIMIT from $item array
                    $soql2->limit($soql_yaml['limit'][0]);
                }

                if (isset($soql_yaml['offset'])) {
                    // Populate the OFFSET from $item array
                    $soql2->offset($soql_yaml['offset'][0]);
                }

              /**
               * Two types of sort specifications can be used. the key
               * "orderlist" is used to denote an a list of column names
               * optionally followed by a direction. For example
               * "salary desc, firstname, lastname". This is the familiar syntax
               * of sql. It is expected that the param contains the literal
               * string that comprises a complete ordering statement.
               *
               * If "orderlist" is used, "order" will be ignored.
               */

              if (isset($soql_yaml['orderlist'])) {
                $list = explode(',', $soql_yaml['orderlist']);
                foreach ($list as $item) {
                  $props = explode(' ', $item);
                  $soql2->order($props[0], $props[1]);
                }
                $has_order = 1;
              }

              else {
                if (isset($soql_yaml['order'])) {

                  /*
                   * Usage example: ORDER must come before GROUP in the YAML sql file
                   *
                   * order:
                   * - { column: class_sort, order: ASC }
                   * - { column: category_sort, order: ASC }
                   * - { column: office_code, order: ASC }
                  */
                  foreach ($soql_yaml['order'] as $sort_column) {
                    if (isset($sort_column['column'])) {
                      $sort_order = SoqlOrderDirection::ASC;
                      if (isset($sort_column['order'])) {
                        if ($sort_column['order'] == 'DESC') {
                          $sort_order = SoqlOrderDirection::DESC;
                        }
                        else {
                          if ($sort_column['order'] == 'ASC') {
                            $sort_order = SoqlOrderDirection::ASC;
                          }
                          else {
                            watchdog('forena_soql', 'Invalid sort order in Forena SOQL yaml query file: ', $sort_column, WATCHDOG_NOTICE);
                          }
                        }
                      }
                      $soql2->order($sort_column['column'], $sort_order);
                      $has_order = 1;
                    }
                    else {
                      watchdog('forena_soql', 'Column not specified in Forena SOQL yaml query file: ', $sort_column, WATCHDOG_NOTICE);
                    };
                  }
                }
              }

                /*
                * Usage example: GROUP must come after ORDER in the YAML sql file
                *
                */
                if (isset($soql_yaml['group'])) {

                    foreach ($soql_yaml['group'] as $sort_column) {
                        if ($has_order == 0) {
                            watchdog('forena_soql', 'If you are using GROUP you must also specify an ORDER column', $item, WATCHDOG_NOTICE);
                        } else {
                            $soql2->group($sort_column);
                        }
                    }
                }

                /* Preforms a full text search on the dataset. Only one parameter is permitted*/
                if (isset($soql_yaml['search'])) {
                    $soql2->fullTextSearch($soql_yaml['search']);
                }


                $pre_xml = $ds->getDataset($soql2);

                $xml2 = toXml($pre_xml);
                $xml = new SimpleXMLElement($xml2);

                if ($this->debug) {
                    if ($xml) $d = htmlspecialchars($xml->asXML());
                    FRx::debug('SQL: ' . $yaml, '<pre> SQL:' . $yaml . "\n XML: " . $d . "\n</pre>");
                }
                return $xml;
            }

        } catch (ParseException $e) {
            watchdog('forena_soql', 'Exception in parsing YAML using Symphony  : ' . $e->getMessage() . '<br/>' . ' YAML: !yaml',
                array(
                    '!yaml' => $yaml
                ),
                WATCHDOG_ERROR);
        } catch (SodaException $e) {
            watchdog('forena_soql', 'Exception in converting YAML to SODA SoQL  : ' . $e->getMessage() . '<br/>' . ' SOQL: !soql' . '<br/>' . ' YAML: !yaml',
                array(
                    '!soql' => $soql2,
                    '!yaml' => json_encode($soql_yaml)
                ),
                WATCHDOG_ERROR);
        } catch (CurlException $e) {
            watchdog('forena_soql', 'Exception in connecting to cURL : ' . $e->getMessage() . '<br/>' . ' Connection: !connection',
                array(
                    '!connection' => json_encode($soql_yaml['connection'])
                ),
                WATCHDOG_ERROR);
            Frx::error('Unable to retrieve the required data. Please try again later or contact PDC for assistance. ');
        } catch (FileNotFoundException $e) {
            watchdog('forena_soql', 'Exception in finding the file : ' . $e->getMessage() . '<br/>' . ' Connection: !connection',
                array(
                    '!connection' => json_encode($soql_yaml['connection'])
                ),
                WATCHDOG_ERROR);
        } catch (HttpException $e) {
            watchdog('forena_soql', 'Exception in HTTP Request/Response : ' . $e->getMessage() . '<br/>' . ' Connection: !connection',
                array(
                    '!connection' => json_encode($soql_yaml['connection'])
                ),
                WATCHDOG_ERROR);
            if (strpos($e->getMessage(), '404') !== false) {
                Frx::error('Unable to retrieve the required data. Please try again later or contact PDC for assistance. ');
            }
        } catch (InvalidResourceException $e) {
            watchdog('forena_soql', 'Exception in finding dataset : ' . $e->getMessage() . '<br/>' . ' Connection: !connection',
                array(
                    '!connection' => json_encode($soql_yaml['connection'])
                ),
                WATCHDOG_ERROR);
        } catch (InvalidSQLWhereSyntax $e) {
            watchdog('forena_soql', ' Exception in where clause of SQL : ' . $e->getMessage(),
                array(),
                WATCHDOG_ERROR);
        } catch (Exception $e) {
            watchdog('forena_soql', ' Exception in YAML - SODA- Socrata conversion : ' . $e->getMessage(),
                array(),
                WATCHDOG_ERROR);
        }
    }

    /*
    Shravanthi - 19 Jun 2017
    Recursive function to construct the where clause from YAML file.
    $clause specifies whether it is inside AND/OR array
    $item_list has the nested array
    Each element is of the structure
    columnName:
        operator: [ = <= >= ], value: [actual value]

    If same column has multiple conditions
     columnName:
       [ {operator: [ = <= >= ], value: [actual value]} ,
        {operator: [ = <= >= ], value: [actual value]} ] */
    public function formWhereClause($item_list, $clause)
    {
        try {
            $finalString = "";
            if (isset($item_list)) {
                foreach ($item_list as $key => $item_val) {
                    $tempString = '';
                    if (is_String($key) && ($key === 'AND' || $key === 'OR')) {
                        if (isset($item_val)) {
                            if (is_array($item_val)) {
                                $tempString .= '(' . $this->formWhereClause($item_val, $key) . ')';
                            } else {
                                throw new InvalidSQLWhereSyntax(" AND/OR is expected to be an array of values ");
                            }
                        }

                    } else {
                        if (isset($item_val)) {
                            if (array_key_exists('operator', $item_val) || array_key_exists('value', $item_val)) {
                                if (isset($item_val['operator']) && isset($item_val['value'])) {
                                    $tempString .= $key . $item_val['operator'] . $item_val['value'];
                                } else {
                                    throw new InvalidSQLWhereSyntax(" key-operator-value syntax is expected. Operator / Value is not set for expected fields ");
                                }
                            } else {
                                /** A field can have more than one condition
                                 * eg: origin ='C3' or origin ='C4'
                                 * In such cases, that particular field is iterated and combined into a string in this else portion
                                 * shravanthi - 5 Jul 2017
                                 */
                                $fieldName = $key;
                                foreach ($item_val as $key => $sub_item_val) {
                                    if (is_int($key)) {
                                        $fieldCondition = $fieldName . $sub_item_val['operator'] . $sub_item_val['value'];
                                        if ($fieldCondition !== '') {
                                            if ($tempString === '') {
                                                $tempString .= $fieldCondition;
                                            } else {
                                                $tempString .= ' ' . $clause . ' ' . $fieldCondition;
                                            }
                                        }
                                    } else {
                                        throw new InvalidSQLWhereSyntax(" key with multiple values expected ");
                                    }
                                }
                            }
                        }
                    }
                    if ($tempString !== '') {
                        if ($finalString === '') {
                            $finalString .= $tempString;
                        } else {
                            $finalString .= ' ' . $clause . ' ' . $tempString;
                        }
                    }
                }
            }
        } catch (InvalidSQLWhereSyntax $e) {
            watchdog('forena_soql', 'Exception in the SQL query WHERE clause: ' . $e->getMessage() . '<br/>' . '!whereClauseItems',
                array(
                    '!whereClauseItems' => $item_list,
                ),
                WATCHDOG_ERROR);

            throw new InvalidSQLWhereSyntax($e->getMessage());
        }

        return $finalString;
    }


    /**
     * Implement custom SQL formatter to make sure that strings are properly escaped.
     * Ideally we'd replace this with something that handles prepared statements, but it
     * wouldn't work for
     *
     * @param unknown_type $value
     * @param unknown_type $key
     * @param unknown_type $data
     */
    public function format($value, $key, $raw = FALSE)
    {
        if ($raw) return $value;
        $value = $this->parmConvert($key, $value);
        if ($value === '' || $value === NULL) {
            $value = 'NULL';
        } elseif (is_array($value)) {
            if ($value == array()) {
                $value = 'NULL';
            } else {
                // Build a array of values string
                $i = 0;
                $val = '';
                foreach ($value as $v) {
                    $i++;
                    if ($i > 1) {
                        $val .= ',';
                    }
                    $val .= "'" . str_replace("'", "''", $v) . "'";
                }
                $value = $val;
            }
        } elseif (is_int($value)) {
            $value = (int)$value;
            $value = (string)$value;
        } elseif (is_float($value)) {
            $value = (float)$value;
            $value = (string)$value;
        } else $value = "'" . str_replace("'", "''", $value) . "'";
        return $value;
    }
}

/**
 * The main function for converting to an XML document.
 * Pass in a multi dimensional array and this recursively loops through and builds up an XML document.
 * @param array $data
 * @param string $rootNodeName - what you want the root node to be - defaults to data.
 * @param SimpleXMLElement $xml - should only be used recursively
 * @return string XML
 * @return string XML
 */
function toXml($data, $rootNodeName = 'table', $xml = null)
{
    try {
        // turn off compatibility mode as simple xml throws a wobbly if you don't.
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }

        if ($xml == null) {
            $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
        }

        // loop through the data passed in.
        foreach ($data as $key => $value) {
            // no numeric keys in our xml please!
            if (is_numeric($key)) {
                // make string key...
                $key = "row_" . (string)$key;
            }

            // if there is another array found recursively call this function
            if (is_array($value)) {
                $node = $xml->addChild($key);
                // recrusive call.
                toXml($value, $rootNodeName, $node);
            } else {
                // add single node.
                $value = htmlspecialchars($value, ENT_QUOTES);
                $xml->addChild($key, $value);
            }

        }
    } catch (Exception $e) {
        watchdog('forena_soql', 'Exception while converting to XML : ' . $e->getMessage() . '<br/>' . ' Data: !data' . '<br/>' . ' XML: !xml',
            array(
                '!data' => $data,
                '!xml' => $xml,
            ),
            WATCHDOG_ERROR);
    }
    // pass back as string. or simple xml object if you want!
    return $xml->asXML();
}

class InvalidSQLWhereSyntax extends Exception
{
}
